# waf

#### 介绍
**access.lua、lib.lua、init.lua**
都是功能实现的lua代码，如果不具备lua的开发能力，我们一般不会去进行改动
**config.lua**为各个功能的配置文件
**rule-config**目录存放了各种防御策略规则
我们需要经常改动**config.lua**和存储策略的文件

#### 软件架构

实现WAF的方式有两种：

1.  使用nginx+lua来实现WAF,须在编译nginx的时候配置上lua

2.  部署OpenResty,不需要在编译nginx的时候指定lua

#### 安装教程
Openresty引入WAF模块

git clone https://gitee.com/jzbee/waf.git

cp -r ./waf /usr/local/openresty/nginx/conf/

#### 使用说明

1.  修改nginx配置来引入WAF模块

2.  如下在Nginx中加入以下配置来引入WAF模块

vim /usr/local/openresty/nginx/conf/nginx.conf

3.  在http部分添加以下

http {

lua_shared_dict limit 10m;

lua_package_path "/usr/local/openresty/nginx/conf/waf/?.lua";

init_by_lua_file "/usr/local/openresty/nginx/conf/waf/init.lua";

access_by_lua_file "/usr/local/openresty/nginx/conf/waf/access.lua";


