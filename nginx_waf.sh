#!/bin/bash
imname=centos
name=centos
confname=nginx.conf
confwafname=waf.conf
docker stop $name
docker rm $name
docker run -dit --restart=always -v /www/wwwroot/centos:/home -p 8181:80 --name $name $imname
docker exec -it $name bash -c "yum -y update"
docker exec -it $name bash -c "yum -y install gcc gcc-c++ autoconf libtool libxslt libxslt-devel libxml2 gd-devel automake make pcre pcre-devel perl-devel perl-ExtUtils-Embed zlib zlib-devel readline readline-devel openssl openssl-devel git vim wget"
docker exec -it $name bash -c "cd /home && wget https://openresty.org/download/openresty-1.19.9.1.tar.gz && tar -zxf openresty-1.19.9.1.tar.gz && cd openresty-1.19.9.1 && ./configure && make -j8 && make install && mkdir -p /usr/local/openresty/nginx/conf/vhost"
docker exec -it $name bash -c "cd /home && git clone https://gitee.com/jzbee/waf.git && ln -sf /home/waf /usr/local/openresty/nginx/conf/ && ln -sf /usr/local/openresty/lualib/resty /home/waf/"
cat > $confwafname << EOF
lua_shared_dict limit 10m;
lua_package_path "/usr/local/openresty/nginx/conf/waf/?.lua";
init_by_lua_file "/usr/local/openresty/nginx/conf/waf/init.lua";
access_by_lua_file "/usr/local/openresty/nginx/conf/waf/access.lua";
server
{
    listen 80;
    server_name _;
    index index.html;
    root html;
    location /wafshow {
        proxy_pass   http://127.0.0.1:8080;
        index  index.html index.htm;
    }
}
EOF
cat > $confname <<'EOF'
user  root;
worker_processes  auto;

error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

pid        logs/nginx.pid;

events {
	use epoll;
       worker_connections 51200;
       multi_accept on;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    gzip on;
    gzip_min_length  1k;
    gzip_buffers     4 16k;
    gzip_http_version 1.1;
    gzip_comp_level 2;
    gzip_types     text/plain application/javascript application/x-javascript text/javascript text/css application/xml;
    gzip_vary on;
    gzip_proxied   expired no-cache no-store private auth;
    gzip_disable   "MSIE [1-6]\.";

    server {
        listen       80;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            root   html;
            index  index.html index.htm;
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

    }

include vhost/*.conf;
}

EOF
did=`docker ps -aqf 'name='${name}`
docker cp $confwafname $did:/usr/local/openresty/nginx/conf/vhost/
docker cp $confname $did:/usr/local/openresty/nginx/conf/
docker exec -it $name bash -c "cd /home && wget http://www.lua.org/ftp/lua-5.1.5.tar.gz && tar -zxf lua-5.1.5.tar.gz && cd lua-5.1.5 && make linux test && make install -j2"
docker exec -it $name bash -c "cd /home && wget https://www.sqlite.org/2021/sqlite-autoconf-3350300.tar.gz && tar -zxf sqlite-autoconf-3350300.tar.gz && cd sqlite-autoconf-3350300 && ./configure && make -j2 && make install"
docker exec -it $name bash -c "cd /home && wget http://luarocks.org/releases/luarocks-2.2.1.tar.gz && tar -zxf luarocks-2.2.1.tar.gz && cd luarocks-2.2.1 && ./configure && make bootstrap"
docker exec -it $name bash -c "luarocks install luasql-sqlite3"
docker exec -it $name bash -c "/usr/local/openresty/nginx/sbin/nginx"
rm -rf $confwafname
rm -rf $confname