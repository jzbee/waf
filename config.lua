--WAF config file,enable = "on",disable = "off"
local current_path = string.sub(debug.getinfo(1).source, 2, string.len("/config.lua") * -1)
--waf base path
config_base_path = current_path
--waf status
config_waf_enable = "on"
--log dir
config_log_dir = current_path.."log"
--rule setting
config_rule_dir = current_path.."rule-config"
-- rule note
config_rule_note = {}
--GeoLite2-City path
config_GeoLite2_path = current_path.."GeoLite2-City.mmdb"
--libmaxminddb path
config_libmaxminddb_path = current_path.."libmaxminddb.so"
-- db path
config_db_path = config_log_dir.."/default.db"
--enable/disable white url
config_white_url_check = "on"
--enable/disable white ip
config_white_ip_check = "on"
--enable/disable block ip
config_black_ip_check = "on"
--enable/disable url filtering
config_url_check = "on"
--enalbe/disable url args filtering
config_url_args_check = "on"
--enable/disable user agent filtering
config_user_agent_check = "on"
--enable/disable cookie deny filtering
config_cookie_check = "on"
--enable/disable cc filtering
config_cc_check = "on"
--cc rate the xxx of xxx seconds
config_cc_rate = "10/60"
--enable/disable post filtering
config_post_check = "on"
--config waf output redirect/html
config_waf_output = "html"
--if config_waf_output ,setting url
config_waf_redirect_url = "localhost"
config_output_html=[[
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-cn" />
<title>防火墙</title>
<style>
*{margin:0;padding:0;color:#444}
body{font-size:14px;font-family:"宋体"}
.main{width:600px;margin:10% auto;}
.title{background: #20a53a;color: #fff;font-size: 16px;height: 40px;line-height: 40px;padding-left: 20px;}
.content{background-color:#f3f7f9; height:280px;border:1px dashed #c6d9b6;padding:20px}
.t1{border-bottom: 1px dashed #c6d9b6;color: #ff4000;font-weight: bold; margin: 0 0 20px; padding-bottom: 18px;}
.t2{margin-bottom:8px; font-weight:bold}
ol{margin:0 0 20px 22px;padding:0;}
ol li{line-height:30px}
</style>
</head>
<body>
	<div class="main">
		<div class="title">防火墙</div>
		<div class="content">
			<p class="t1">请求出错了</p>
			<p class="t2">可能原因：</p>
			<ol>
				<li>欢迎白帽子进行授权安全测试，安全漏洞请联系</li>
			</ol>
			<p class="t2">如何解决：</p>
			<ol>
				<li>请联系管理员</li>
			</ol>
		</div>
	</div>
</body>
</html>
]]